import sys
import os
import time
import json
import urllib2
import datetime
import traceback
import xml.etree.ElementTree
from threading import Thread

sys.path.append('/home/agrin/ProjectsEnc/')
import botlib

def get_safe_response_data(url):
  try:
    response = urllib2.urlopen(url, timeout=1)
    response_data = json.loads(response.read())
    return response_data
  except:
    time.sleep(3)
    return {}


def update_data_thread_function(updates_settings, cash_file_name):
  global updating_data
  last_update_datetime = datetime.datetime.now() - datetime.timedelta(weeks=10)
  while True:
    new_datetime = datetime.datetime.now()
    if new_datetime - last_update_datetime > updates_settings['frequency']:
      print('Updating data at datetime: {}', new_datetime)
      last_update_datetime = datetime.datetime.now()
      try:
        new_data = botlib.utils.update_data(updates_settings)
        if cash_file_name:
          botlib.utils.update_cash_file(new_data, cash_file_name)
          updating_data = json.load(open(cash_file_name))
        else:
          updating_data = new_data
      except urllib2.HTTPError:
        print('HTTPError')
        time.sleep(3)
        continue
      except urllib2.URLError:
        print('URLError')
        time.sleep(3)
        continue
    time.sleep(10)


def main_loop_webhooks(generate_model_func, generate_response_func,
                       updates_settings={'frequency':None, 'data':{}}, **kwargs):
  """
  updates_settings format:
  {frequency: datetime.timedelta,
   data: {first_field: first_function, second_field: second_function, ...}}

  kwargs:
  cash_file_name: str, cash file name for cashing updates data, None for no file name
  """
  cash_file_name = kwargs.get('cash_file_name', None)
  list_of_sources = kwargs['list_of_sources'] # .get('list_of_sources', [])
  model = generate_model_func()
  print('Model trained')
  
  global updating_data
  if os.path.isfile(cash_file_name):
    updating_data = json.load(open(cash_file_name))
  else:
    updating_data = {}
  update_data_thread = Thread(target=update_data_thread_function,
                              args=(updates_settings, cash_file_name))
  update_data_thread.start()

  current_update_id = 0
  activity_status = None
  last_ping_time = datetime.datetime.now()
  while True:
    for source in list_of_sources:
      current_datetime = datetime.datetime.now()
      if source['node'] == 'https://api.telegram.org/' and activity_status != 'waiting':
        try:
          message_data = botlib.utils.get_last_message_data(source['id']['token'])
        except urllib2.HTTPError:
          print('HTTPError')
          time.sleep(3)
          continue
        except urllib2.URLError:
          print('URLError')
          time.sleep(3)
          continue
        input_data = botlib.utils.message_to_input(message_data)

        try:
          if input_data['update_id'] != None:
            botlib.utils.clear_updates(source['id']['token'], input_data['update_id'])
        except:
          print('Updates clearing failed')
        if (input_data['update_id'] != None and input_data['message_text'] != None
            and (current_update_id == None or input_data['update_id'] > current_update_id)):

          current_update_id = input_data['update_id']
          analizing_data = {'message_text':input_data['message_text'],
                            'username':input_data['username'],
                            'first_name':input_data['first_name'],
                            'last_name':input_data['last_name'],
                            'recipient':{'channel':'telegram',
                                         'token':source['id']['token'],
                                         'chat_id':input_data['chat_id']}}
          try:
            response = generate_response_func(input_data['message_text'], model,
                                              updating_data, updating_data)
          except:
            response = u'Sorry, can\'t reply'
          botlib.utils.write_message(response,
                                     analizing_data['recipient']['chat_id'],
                                     analizing_data['recipient']['token'])

          botlib.utils.write_log(file_name='./data/log.txt',
                                 message_text=analizing_data['message_text'],
                                 response=response,
                                 channel=analizing_data['recipient']['channel'],
                                 chat_id=analizing_data['recipient']['chat_id'],
                                 username=analizing_data['username'],
                                 first_name=analizing_data['first_name'],
                                 last_name=analizing_data['last_name'])



      else:
        current_time = datetime.datetime.now()
        if current_time - last_ping_time > datetime.timedelta(seconds=10):
          last_ping_time = current_time

          response_data = get_safe_response_data(source['node'] + 'ping' + source['id']['token']
                                                 + '/getupdates')
          if response_data.get('sysmsg') == 'ping':
            activity_status = response_data.get('status')
            analizing_data = {'node':source['node'],
                              'recipient':source['id'], 'ping':True}
            http_address = (analizing_data['node'] + 'ping' + analizing_data['recipient']['token']
                            + '/sendmessage')
            try:
              urllib2.urlopen(http_address, timeout=1)
            except:
              pass


# Obtains currency exchange rates via new yahoo API
def get_currencies_yahoo_new():
  request_url = 'https://finance.yahoo.com/webservice/v1/symbols/allcurrencies/quote'
  xml_output = urllib2.urlopen(request_url).read()
  result = dict()
  for child in xml.etree.ElementTree.fromstring(xml_output)._children[1]._children[0]:
    name = child[0].text
    rate = child[1].text
    result[name] = rate
  return result



